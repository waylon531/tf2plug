# Package

version       = "0.1.3"
author        = "Waylon Cude"
description   = "A mod manager for TF2"
license       = "GPLv3"

bin = @["tf2plug"]

# Dependencies

requires "nim >= 0.17.0"
requires "docopt >= 0.6.5"
