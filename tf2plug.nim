#    TF2Plug, a program to manage tf2 mods
#    Copyright (C) 2017 Waylon Cude
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    

import os
import ospaths
import logging
import tables
import strutils
import docopt
import marshal

proc extractDir(tf2_custom, name: string, target = "") =
  ## Move actual gui directory out of downloaded files
  let tempdir = tf2_custom & "___temp_" & name
  let namedir = tf2_custom & name
  let target = if target == "": tf2_custom & name & "/" & name
    else: tf2_custom & name & "/" & target
  copyDir(target, tempdir)
  removeDir(namedir)
  copyDir(tempdir, namedir)
  removeDir(tempdir)

#
# Setup docopts
#
let doc = """
TF2Plug.

Usage:
  tf2plug [options] install <url>
  tf2plug [options] update [<name>]
  tf2plug [options] ( remove | uninstall ) <name>
  tf2plug (-h | --help)
  tf2plug --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  -v --verbose  Enable verbose output.
"""


let args = docopt(doc, version = "TF2Plug 0.1.3")

#
# Initialize logger
#

# How can I make this immutable?
var log: Logger;
if args["--verbose"]:
  log = newConsoleLogger(lvlInfo)
else:
  log = newConsoleLogger(lvlError)

addHandler(log)


#
# Initialize program
#
let home = os.getHomeDir()

var data_home = os.getEnv("XDG_DATA_HOME")

if data_home == "":
    data_home = home & "/.local/share"

if not os.existsOrCreateDir(data_home & "/tf2plug"):
    info("Created data directory")

let data_dir = data_home & "/tf2plug"
#
# Find tf2 directory
#
if not os.fileExists(data_dir & "/tf2location"):
  if os.dirExists(home & "/.steam/steam/SteamApps/common/Team Fortress 2"):
    writeFile(data_dir & "/tf2location", home & "/.steam/steam/SteamApps/common/Team Fortress 2")
    info("TF2 found at ~/.steam/steam/SteamApps/common/Team Fortress 2")
  elif os.dirExists(home & "/.steam/steam/steamapps/common/Team Fortress 2"):
    writeFile(data_dir & "/tf2location", home & "/.steam/steam/steamapps/common/Team Fortress 2")
    info("TF2 found at ~/.steam/steam/steamapps/common/Team Fortress 2")
  else:
    # TF2Plug could not guess 
    echo "TF2 directory not found, input path:"
    var dir = readLine(stdin)
    while (not os.dirExists(dir)):
      echo "Directory not found, please enter again:"
      dir = readLine(stdin)
    writeFile(data_dir & "/tf2location", dir)
    info("TF2 found at $#".format(dir))

let tf2_location = readFile(data_dir & "/tf2location")
info("Using TF2 at $#".format(tf2_location))

if not os.existsOrCreateDir(tf2_location & "/tf/custom"):
    info("Created tf/custom directory")

let tf2_custom = tf2_location & "/tf/custom/"

#
# Find existing plugins
#
var plugins: Table[string,string]
if not os.fileExists(data_dir & "/plugins"):
  plugins = initTable[string,string]()
else:
  plugins = marshal.to[Table[string,string]](readFile(data_dir & "/plugins"))

#
# Install plugin
#
if args["install"]:
  var url: string;
  case ($args["<url>"]).toLowerAscii()
  of "rayshud":
    url = "https://github.com/raysfire/rayshud"
  of "7hud":
    url = "https://github.com/Sevin7/7HUD"
  of "budhud":
    url = "https://github.com/rbjaxter/budhud"
  of "medhud":
    url = "https://github.com/Intellectualbadass/medHUD"
  else:
    url = $args["<url>"]

  let name = splitFile(url).name.toLowerAscii()
  if plugins.hasKey(name):
    error("$# already installed".format(name))
    quit(1)

  info("Installing $# into $#".format(name,tf2_custom & name))
  # Install into folder
  let result = os.execShellCmd("""git clone $# "$#"""".format(url,tf2_custom & name))
  if result != 0:
    error("Installation failed")
    quit(1)
  else:
    if name == "budhud":
      tf2_custom.extractDir(name)
    info("Installation succeeded")
    plugins[name] = tf2_custom & name
    writeFile(data_dir & "/plugins",$$plugins)
    echo "Installed $#".format(name)

#
# Remove plugin
#
if args["remove"] or args["uninstall"]:
  let name = ($args["<name>"]).toLowerAscii()
  if not plugins.hasKey(name):
    error("$# not installed, cannot uninstall".format(name))
    quit(1)
  else:
    info("Removing $# at $#".format(name,plugins[name]))
    removeDir(plugins[name])
    info("Removed $#".format(name))
    plugins.del(name)
    writeFile(data_dir & "/plugins",$$plugins)
    echo "Removed $#".format(name)


#
# Update plugin
#
if args["update"]:
  if args["<name>"]:
    let name = ($args["<name>"]).toLowerAscii()
    if not plugins.hasKey(name):
      error("$# not installed, cannot update".format(name))
      quit(1)
    let result = os.execShellCmd("""cd "$#" && git pull""".format(plugins[name]))
    if result != 0:
      error("Update failed")
      quit(1)
    else:
      info("Update succeeded")
  else:
    for k,v in plugins:
      info("Updating $#".format(k))
      let result = os.execShellCmd("""cd "$#" && git pull""".format(v))
      if result != 0:
        warn("Updating $# failed".format(k))
      else:
        info("Update succeeded")

